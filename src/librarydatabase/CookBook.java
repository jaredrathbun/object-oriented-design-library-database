package librarydatabase;

/**
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 9/9/20
 *
 * This class holds the data for a CookBook, which is inherited from 
 * NonFictionBook.
 */
public class CookBook extends NonFictionBook
{
    private String topic; // The topic of the book.
    
    /**
     * Constructor that calls the super class (NonFictionBook).
     * 
     * @param title The title of the CookBook.
     * @param publisher The publisher of the CookBook.
     * @param pageCount The number of pages in the CookBook.
     * @param language The language of the CookBook.
     * @param topic The topic of the CookBook.
     */
    public CookBook(String title, String publisher, int pageCount, 
            String language, String topic)
    {
        super(title, publisher, pageCount, language);
        this.topic = topic;
    }
   
    /**
     * Getter for the topic field.
     * 
     * @return The topic of CookBook. 
     */
    public String getTopic()
    {
        return topic;
    }

    /**
     * Setter for the topic field.
     * 
     * @param topic The topic of the CookBook. 
     */
    public void setTopic(String topic)
    {
        this.topic = topic;
    }
    
    /**
     * toString method for the CookBook class.
     * 
     * @return The title + the language of the CookBook.
     */
    @Override
    public String toString()
    {
        return String.format("| %40s| %20s| %20s| %15s| %30s| %30s|", 
                super.getTitle(), super.getLanguage(), super.getPublisher(), 
                super.getPageCount(), topic, "");
    }
    
    /**
     * This method checks if the fields of the class contain the substring.
     * 
     * @param substring The string to search for.
     * @return true if the condition is satisfied, false otherwise. 
     */
    @Override
    public boolean hasSubstring(String substring)
    {
        return (super.getTitle().contains(substring) || 
                super.getPublisher().contains(substring) || 
                topic.contains(substring));
    }
}
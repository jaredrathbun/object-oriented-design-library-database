package librarydatabase;

/**
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 9/9/20
 *
 * This class holds the data for a Novel, which inherits from FictionBook.
 */
public class Novel extends FictionBook
{
    // A boolean for if the Novel is part of a series or not.
    private boolean isPartOfSeries; 

    /**
     * Constructor that calls the super class and also sets attributes.
     * 
     * @param title The title of the Novel.
     * @param publisher The publisher of the Novel.
     * @param pageCount The number of pages in the Novel.
     * @param author The author of the Novel.
     * @param genre The genre of the Novel.
     * @param isPartOfSeries A flag declaring if the Novel is part of a series.
     */
    public Novel(String title, String publisher, int pageCount, String author, 
            String genre, boolean isPartOfSeries)
    {
        super(title, publisher, pageCount, author, genre);
        this.isPartOfSeries = isPartOfSeries;
    }

    /**
     * Setter for isPartOfSeries.
     * 
     * @param isPartOfSeries A char declaring if the novel is part 
     * of a series.
     */
    public void setIsPartOfSeries(boolean isPartOfSeries)
    {
        this.isPartOfSeries = isPartOfSeries;
    }
    
    /**
     * Getter for the isPartOfSeries field.
     * 
     * @return A char containing if the Novel is part of a series or not.
     */
    public boolean getIsPartOfSeries()
    {
        return isPartOfSeries;
    }
    
    /**
     * toString method for the Novel class.
     * 
     * @return A neatly printed right aligned version of the object's attributes.
     */
    @Override
    public String toString()
    {      
        char isPartOf;
        
        if (isPartOfSeries)
            isPartOf = 'Y';
        else
            isPartOf = 'N';
                    
        return String.format("| %40s| %20s| %20s| %15s| %30s| %30s|", 
                super.getTitle(), super.getAuthor(), super.getPublisher(), 
                super.getPageCount(), super.getGenre(), isPartOf);
    }
    
    /**
     * This method checks if the attributes of this class contain the substring.
     * 
     * @param substring The substring to check for.
     * @return True if the comparison is satisfied, false otherwise.
     */
    @Override
    public boolean hasSubstring(String substring)
    {
        return (super.getTitle().contains(substring) || 
                super.getPublisher().contains(substring));
    }
}
package librarydatabase;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 9/9/20
 *
 * This program demonstrates the use of Inheritance and Polymorphism. It also 
 * allows the storing a library via a HashMap, and searching via a GUI.
 */
public class Driver 
{
    // A HashMap to hold the contents of the library.
    private static HashMap<String,Book> library;
    
    public static void main(String[] args) 
    {

        // Initialize the map.
        library = new HashMap<>();
        
        readFile("library.txt");
        
        // Prompt the user and display the library using their input.
        displayLibrary(popMenu());          
    }
    
    /**
     * This method reads a text file that contains the contents the of the 
     * library and then inputs them into the HashMap.
     * 
     * @param path The path of the file (a string). 
     */
    private static void readFile(String path)
    {
        assert (path == null); // Precondition check.
        
        Scanner input = null;
        
        try
        {
            input = new Scanner(new File(path));
        } catch (FileNotFoundException ex) {
            System.out.println("Error reading text file. Exiting program.");
            System.out.println(ex.getMessage());
            System.exit(0);
        }

        /* Take the first line (specifies how many objects will be created,
           and swallow it */
        input.nextLine();
        
        while(input.hasNextLine())
        {
            /* Convert each component of the line into it's own element in an 
                array, ignoring the ',' between each word. */
            Object[] contents = input.nextLine().split(", ");
            
            switch (Integer.parseInt((String) contents[0]))
            {
                case 1:
                {   
                    // Get the title.
                    String title = String.valueOf(contents[1]); 

                    // Create the new object.
                    Dictionary dictionary = new Dictionary(
                            title, String.valueOf(contents[2]),
                            Integer.parseInt((String) contents[3]),
                            String.valueOf(contents[4]),
                            Integer.parseInt((String) contents[5]));

                    // Put the new Dictionary into the map.
                    library.put(title, dictionary);
                    break;
                }
                case 2:
                {   
                    // Get the title.
                    String title = String.valueOf(contents[1]); 

                    // Create the new object.
                    CookBook cookBook = new CookBook(
                            title, String.valueOf(contents[2]),
                            Integer.parseInt((String) contents[3]),
                            String.valueOf(contents[4]),
                            String.valueOf(contents[5]));

                    // Put the new CookBook into the map.
                    library.put(title, cookBook);
                    break;
                }
                case 3:
                {   
                    // Get the title.
                    String title = String.valueOf(contents[1]); 
                    
                    boolean isPartOf = 
                            String.valueOf(contents[6]).charAt(0) == 'Y';
                    
                    // Create the new object.
                    Novel novel = new Novel(
                            title, String.valueOf(contents[2]),
                            Integer.parseInt((String) contents[3]),
                            String.valueOf(contents[4]),
                            String.valueOf(contents[5]),
                            isPartOf);

                    // Put the new Novel into the map.
                    library.put(title, novel);
                    break;
                }
                case 4:
                {
                    // Get the title.
                    String title = String.valueOf(contents[1]); 

                    // Create the new object.
                    GraphicNovel gn = new GraphicNovel(
                            title, String.valueOf(contents[2]),
                            Integer.parseInt((String) contents[3]),
                            String.valueOf(contents[4]),
                            String.valueOf(contents[5]), 
                            String.valueOf(contents[6]));

                    // Add the new GraphicNovel to the map.
                    library.put(title, gn);
                    break;
                }
            }
        }
    }
    
    /**
     * This method displays a menu to user asking for a decision on the 
     * operation they would like to perform.
     * 
     * @return A number between 1 and 5, 1 for the whole library, and 2-4 for 
     * a specified version of the library and 5 for a search of the library by 
     * a specified string.
     */
    private static int popMenu()
    {
        Scanner scanner = new Scanner(System.in);
        
        int input; // The option the user enters.
        
        do
        {
            System.out.println("Please select one of the following options: ");
            System.out.println("Enter '1' to display to contents of the "
                    + "library.");
            System.out.println("Enter '2' to search the library by author.");
            System.out.println("Enter '3' to search the library by genre.");
            System.out.println("Enter '4' to search the library by language.");
            System.out.println("Enter '5' to search by substring.");
            System.out.println("Enter '6' to quit.");
            System.out.print("Please enter your choice: ");
            
            input = scanner.nextInt();
        
        } while (input < 0 || input > 6);
        
        return input;
    }
    
    /**
     * This method prints the library, sorting by the specified choice the 
     * user selects. Here's what it does:
     * '1': Unsorted library
     * '2': Sorts the library by a genre, author or language.
     * '3': Search the library for contents by a substring.
     * 
     * @param choice The operation to perform.
     */
    private static void displayLibrary(int choice)
    {
        assert (choice > 1 && choice < 7); // Precondition check.
        
        System.out.println(""); // Print a blank line.
        
        switch (choice)
        {
            case 1:
            {           
                // Print a header.
                printHeader();
                
                ArrayList<Book> list = new ArrayList<>();
                
                // Traverse the values of the map and print them.
                for (Object obj : library.values())
                {
                    list.add((Book) obj);
                }
                
                Collections.sort(list); // Sort the ArrayList.
                
                // Traverse the ArrayList and print each element.
                for (Object obj : list)
                {
                    System.out.println(obj);
                }
                
                break;
            }
            
            case 2:
            {
                ArrayList<String> list = new ArrayList<>();

                for (Object obj : library.values())
                {    
                    if (obj instanceof FictionBook)
                    {
                        /* If the author is not already in the ArrayList, 
                        add it. (avoids duplicates) */
                        if (!list.contains(((FictionBook) obj).getAuthor()))
                        {
                            list.add(((FictionBook) obj).getAuthor());
                        }
                    }
                }
                
                // Prompt the user for the author.
                String author = getUserChoice(list);
                
                printHeader();
                
                /* Travere the map and if the current object contains the 
                search, print it. */
                for (Object obj : library.values())
                {
                    if (obj instanceof FictionBook)
                    {
                        if (((FictionBook) obj).getAuthor().equals(author))
                            System.out.println(obj);
                    }
                }
                break;
            }
            
            case 3:
            {
                ArrayList<String> list = new ArrayList<>();
                
                for (Object obj : library.values())
                {
                    if (obj instanceof FictionBook)
                    {
                        /* If the genre is not already in the ArrayList, add it.
                        (avoids duplicates) */
                        if (!list.contains(((FictionBook) obj).getGenre()))
                            list.add(((FictionBook) obj).getGenre());
                    }
                }
                
                // Prompt the user for the genre.
                String genre = getUserChoice(list);
                
                printHeader();
                
                for (Object obj : library.values())
                {
                    /* If the object is a FictionBook, check if the genre of 
                    the object equals the genre the user entered. */
                    if (obj instanceof FictionBook)
                    {
                        if (obj instanceof GraphicNovel)
                        {
                            if (((GraphicNovel) obj).getGenre().equals(genre))
                                System.out.println(obj);
                        }
                        
                        if (obj instanceof Novel)
                        {
                            if (((Novel) obj).hasSubstring(genre))
                                System.out.println(obj); 
                        }  
                    }
                }
                
                break;
            }     
            
            case 4: 
            {
                ArrayList<String> list = new ArrayList<>();
                
                for (Object obj : library.values())
                {
                    if (obj instanceof NonFictionBook)
                    {
                        /* If the language is not already added to the 
                        ArrayList, add it. (avoids duplicates) */
                        if (!list.contains(((NonFictionBook) obj)
                                .getLanguage()))
                            list.add(((NonFictionBook) obj).getLanguage());
                    }
                }
                
                // Get the language to search for.
                String language = getUserChoice(list);
                
                printHeader(); // Print the header.
                
                /* Travere the map and if the current object contains the 
                search, print it. */
                for (Object obj : library.values())
                {
                    if (obj instanceof NonFictionBook)
                    {
                        if (((NonFictionBook) obj).hasSubstring(language))
                            System.out.println(obj);
                    }
                }
                
                break;
            }
            
            case 5:
            {
                // Get the string to search for.
                String substring = getSubstring();
                
                // A counter for how many results appear.
                int count = 0; 
                
                // An ArrayList to store the books that match the search.
                ArrayList<Object> list = new ArrayList<>();
                
                /* Traverse the HashMap and if the current object contains the
                substring, add it to the ArrayList and increment count. */
                for (Object obj : library.values())
                {
                    if (((Book) obj).hasSubstring(substring))
                    {
                        list.add((Book) obj);
                        count++;
                    }
                }
                
                // Print a header.
                System.out.println("\nHere are the " + count + " result(s) "
                        + "for the search: " + substring);
                
                printHeader();
                
                /* Loop through the ArrayList of Books that match the search 
                and print them */
                for (Object obj : list)
                    System.out.println(obj);
                break;
            }
            
            case 6:
            {
                // Exit the program.
                System.exit(0);
                break;
            }
        }
    }

    /**
     * This method prompts the user for a substring to search for in 
     * the HashMap.
     * 
     * @return A string containing the substring the user entered. 
     */
    private static String getSubstring()
    {
        Scanner input = new Scanner(System.in);
        String key;
        
        // Prompt the user for a string.
        do
        {
           System.out.print("Please enter the substring you would like to "
                + "search by: "); 
           key = input.next();
           
        } while (key.equals(""));
        
        return key;
    }
    
    /**
     * This method prints a header for the library of books that is 
     * right aligned.
     */
    private static void printHeader()
    {
        System.out.printf("| %40s| %20s| %20s| %15s| %30s| %30s|\n", "Title", 
                        "Author/Language", "Publisher", "Page Count", 
                        "Version Number/Genre/Topic", 
                        "Part of Series/Illustrator");
                
        for (int i = 0 ; i < 168; i++)
            System.out.print("-");
        System.out.println("");
    }
    
    /**
     * This method prompts the user for a choice from the ArrayList passed as
     * a parameter.
     * 
     * @param list The list of Books to display.
     * @return A string containing the user's choice.
     */
    private static String getUserChoice(ArrayList<String> list) 
    {   
        Scanner input = new Scanner(System.in);
        
        for (int i = 1; i <= list.size(); i++)
            System.out.println(i + ": " + list.get(i-1));
        
        int userInput = 0;
        do 
        {
            System.out.print("Please enter your choice: ");
            userInput = input.nextInt();
        } 
        while (userInput < 0 || userInput > list.size());
        
        return list.get(userInput - 1);
    }
}
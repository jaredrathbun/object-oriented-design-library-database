package librarydatabase;

/**
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 9/9/20
 *
 * This class stores the data for a GraphicNovel, which inherits from 
 * FictionBook.
 */
public class GraphicNovel extends FictionBook
{
    private String illustrator; // The illustrator of the GraphicNovel.

    /**
     * Constructor that calls the super class (FictionBook) and also 
     * sets illustrator.
     * 
     * @param title The title of the GraphicNovel.
     * @param publisher The publisher of the GraphicNovel.
     * @param pageCount The number of pages in the GraphicNovel.
     * @param author The author of the GraphicNovel.
     * @param genre The genre of the GraphicNovel.
     * @param illustrator The illustrator of the GraphicNovel.
     */
    public GraphicNovel(String title, String publisher, int pageCount, 
            String author, String genre, String illustrator)
    {
        super(title, publisher, pageCount, author, genre);
        this.illustrator = illustrator;
    }

    /**
     * Getter for Illustrator field.
     * 
     * @return The Illustrator of the GraphicNovel.
     */
    public String getIllustrator()
    {
        return illustrator;
    }

    /**
     * Setter for the Illustrator field.
     * 
     * @param illustrator The illustrator of the GraphicNovel. 
     */
    public void setIllustrator(String illustrator)
    {
        this.illustrator = illustrator;
    }
    
    /**
     * toString method for the GraphicNovel.
     * 
     * @return The title plus the illustrator of the GraphicNovel.
     */
    @Override
    public String toString()
    {
        return String.format("| %40s| %20s| %20s| %15s| %30s| %30s|", 
                super.getTitle(), super.getAuthor(), super.getPublisher(), 
                super.getPageCount(), super.getGenre(), illustrator);
    }
    
    /**
     * This method checks if the attributes of this class contain the substring.
     * 
     * @param substring The substring to check for.
     * @return True if the comparison is satisfied, false otherwise.
     */
    @Override
    public boolean hasSubstring(String substring)
    {
        return (super.getTitle().contains(substring) || 
                super.getPublisher().contains(substring) || 
                illustrator.contains(substring));
    }
}
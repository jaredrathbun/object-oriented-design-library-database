package librarydatabase;

/**
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 9/9/20
 *
 * This class holds the data for FictionBook, which inherits from Book.
 */
public class FictionBook extends Book implements Comparable<Book>
{
    private String author; // The author of the FictionBook.
    private String genre; // The genre of the FictionBook.

    /**
     * Constructor for the FictionBoook class.
     * 
     * @param title The title of the Book.
     * @param publisher The publisher of the Book.
     * @param pageCount The number of pages the Book contains.
     * @param author The author of the Book.
     * @param genre The genre of the Book.
     */
    public FictionBook(String title, String publisher, int pageCount, 
            String author, String genre)
    {
        super(title, publisher, pageCount);
        this.author = author;
        this.genre = genre;
    }

    /**
     * Getter for the author field.
     * 
     * @return The author of the Book.
     */
    public String getAuthor()
    {
        return author;
    }

    /**
     * Setter for the author field.
     * 
     * @param author The author of the Book. 
     */
    public void setAuthor(String author)
    {
        this.author = author;
    }

    /**
     * Getter for the genre field.
     * 
     * @return The genre of the Book. 
     */
    public String getGenre()
    {
        return genre;
    }

    /**
     * Setter for the genre field.
     * 
     * @param genre The genre of the Book.
     */
    public void setGenre(String genre)
    {
        this.genre = genre;
    }
    
    /**
     * toString method for the FictionBook.
     * 
     * @return The author of the FictionBook.
     */
    @Override
    public String toString()
    {
        return author;
    }
    
    /**
     * This method compares the author of this instance of FictionBook and o, 
     * another Book. If the authors are lexicographically the same, 
     * the tie is broken by the title of the two.
     * 
     * @param o The other Book to compare to.
     * @return An integer containing the comparison.
     */
    @Override
    public int compareTo(Book o) 
    {
        int langCompare = 0;
                
        // If the book is of type FictionBook, compare the authors.
        if (o instanceof FictionBook)
        {
             langCompare = author.compareTo(
                    ((FictionBook) o).getAuthor());
        }
        
        // If the book is of type NonFictionBook, compare the Languages.
        if (o instanceof NonFictionBook)
        {
            return author.compareTo(((NonFictionBook) o).getLanguage());
        }
        
        // Break the tie by comparing the titles of the books.
        if (langCompare == 0)
            return getTitle().compareTo(o.getTitle());     

        return langCompare;
    }
    
    /**
     * This method checks if the attributes of this class contain the substring.
     * 
     * @param substring The substring to check for.
     * @return True if the comparison is satisfied, false otherwise.
     */
    @Override
    public boolean hasSubstring(String substring)
    {
        return (super.getTitle().contains(substring) || 
                super.getPublisher().contains(substring) || 
                author.contains(substring) || genre.contains(substring));
    }
}
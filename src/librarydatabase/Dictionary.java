package librarydatabase;

/**
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 9/9/20
 *
 * This class holds the data for a Dictionary, which inherits from 
 * NonFictionBook.
 */
public class Dictionary extends NonFictionBook
{
    private int versionNumber; // The version of the Book.

    /**
     * Constructor that calls super class.
     * 
     * @param title The title of the Dictionary.
     * @param publisher The publisher of the Dictionary.
     * @param pageCount The number of the pages the Dictionary contains.
     * @param language The language the Dictionary is written in.
     * @param versionNumber The version of the Dictionary.
     */
    public Dictionary(String title, String publisher, int pageCount, 
            String language, int versionNumber)
    {
        super(title, publisher, pageCount, language);
        this.versionNumber = versionNumber;
    }

    /**
     * Getter for versionNumber field.
     * 
     * @return The version of the Dictionary. 
     */
    public int getVersionNumber()
    {
        return versionNumber;
    }

    /**
     * Setter for versionNumber field.
     * 
     * @param versionNumber The version of the Dictionary. 
     */
    public void setVersionNumber(int versionNumber)
    {
        this.versionNumber = versionNumber;
    }
    
    /**
     * toString method for the Dictionary.
     * 
     * @return The title of the dictionary plus the language.
     */
    @Override
    public String toString()
    {
        return String.format("| %40s| %20s| %20s| %15s| %30s| %30s|", 
                super.getTitle(), super.getLanguage(), super.getPublisher(), 
                super.getPageCount(), versionNumber, "");
    }
}
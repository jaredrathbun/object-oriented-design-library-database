package librarydatabase;

/**
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 9/9/20
 *
 * This class is the top class of the hierarchy tree for this project. 
 * It holds the basic data for any Book.
 */
public abstract class Book implements Comparable<Book>
{
    private String title; // The title of the book.
    private String publisher; // The publisher of the book.
    private int pageCount; // The number of pages to book has.  

    /**
     * Constructor for the super class of the project. 
     * 
     * @param title The title of the Book.
     * @param publisher The publisher of the Book.
     * @param pageCount The number of pages in the Book.
     */
    public Book(String title, String publisher, int pageCount)
    {
        this.title = title;
        this.publisher = publisher;
        this.pageCount = pageCount;
    }

    /**
     * Getter for title field.
     * 
     * @return The title of the book.
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Setter for title field.
     * 
     * @param title The title of the book. 
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Getter for publisher field.
     * 
     * @return The publisher of the book. 
     */
    public String getPublisher()
    {
        return publisher;
    }

    /**
     * Setter for publisher field.
     * 
     * @param publisher The publisher of the book.
     */
    public void setPublisher(String publisher)
    {
        this.publisher = publisher;
    }

    /**
     * Getter for pageCount field.
     * 
     * @return The number of pages the book contains.
     */
    public int getPageCount()
    {
        return pageCount;
    }

    /**
     * Setter for pageCount field.
     * 
     * @param pageCount The number of pages the book contains.
     */
    public void setPageCount(int pageCount)
    {
        this.pageCount = pageCount;
    }
    
    /**
     * toString method for Book.
     * 
     * @return The title of the Book.
     */
    @Override
    public String toString()
    {
        return title;
    }
    
    /**
     * This method checks if the attributes of the class contain the substring 
     * passed as a parameter.
     * 
     * @param substring The string to search for.
     * @return true if the condition is satisfied, false otherwise.
     */
    public boolean hasSubstring(String substring)
    {
        return (title.contains(substring) || publisher.contains(substring));
    }
}
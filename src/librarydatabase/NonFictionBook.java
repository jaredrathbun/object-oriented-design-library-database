package librarydatabase;

/**
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 9/9/20
 *
 * This class holds the data for a NonFictionBook, which inherits from Book.
 */
public class NonFictionBook extends Book implements Comparable<Book>
{
    private String language; // The language of the NonFictionBook.

    /**
     * Constructor that calls the super class (Book).
     * 
     * @param title The title of the Book.
     * @param publisher The publisher of the Book.
     * @param pageCount The number of pages in the Book.
     * @param language The language the NonFictionBook is written in. 
     */
    public NonFictionBook(String title, String publisher, int pageCount, 
            String language)
    {
        super(title, publisher, pageCount);
        this.language = language;
    }

    /**
     * Getter for language.
     * 
     * @return A string containing the language the book is written in.
     */
    public String getLanguage()
    {
        return language;
    }

    /**
     * Setter for language.
     * 
     * @param language A string containing the language the book is written in.
     */
    public void setLanguage(String language)
    {
        this.language = language;
    }
    
    /**
     * This method compares the language of this instance of NonFictionBook 
     * and o, another Book. If the languages are lexicographically the same, 
     * the tie is broken by the title of the two.
     * 
     * @param o The other Book to compare to.
     * @return An integer containing the comparison.
     */
    @Override
    public int compareTo(Book o)
    {
        int langCompare = 0;
                
        // If the book is of type NonFictionBook, compare the languages.
        if (o instanceof NonFictionBook)
        {
             langCompare = language.compareTo(
                    ((NonFictionBook) o).getLanguage());
        }
        
        /* If the book is of type FictionBook, 
        compare the language to the author. */
        if (o instanceof FictionBook)
        {
            return language.compareTo(((FictionBook) o).getAuthor());
        }
        
        // Break the tie by comparing the titles of the books.
        if (langCompare == 0)
            return getTitle().compareTo(o.getTitle());     

        return langCompare;
    }
    
    /**
     * This method checks if the attributes of this class contain the substring.
     * 
     * @param substring The substring to check for.
     * @return True if the comparison is satisfied, false otherwise.
     */
    @Override
    public boolean hasSubstring(String substring)
    {
        return (super.getTitle().contains(substring) || 
                super.getPublisher().contains(substring) || 
                language.contains(substring));
    }
}